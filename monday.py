from __future__ import print_function
import requests
import gspread
import datetime
import pytz

# Extract dos quadros da api do monday
def get_request(board_id, key):
    str_id = '(ids: {})'.format(board_id)
    query = '{boards' + str_id + '{name id groups {title id} items {name group {title id} column_values {title id value type text} } } }'
    url = "https://api.monday.com/v2"
    headers = {"Authorization": key}

    data = {'query': query}
    r = requests.post(url=url, json=data, headers=headers)
    return r


# Transform
def get_monday(items_json, table, titles):  # Função para retornar meta, colunas com valores
    c_output = []
    t_output = []
    for item in items_json:
        if table not in item['group']['title']:
            continue
        name = item['name']
        t_output.append(name)
        columns = item['column_values']  # Selecionando colunas
        column_dict = {}
        for column in columns:
            if column['title'] in titles:
                if column['value'] is None:
                    column['value'] = '0'
                column['value'] = column['value'].strip('"')
            column_dict[column['title']] = column['value']
            c_output.append(column_dict)
    t = (dict(zip(t_output, c_output)))
    return t


def get_table_name(groups_json):  # Função para retornar todos os grupos
    g_output = []
    for groups in groups_json:
        groups_names = groups['title']
        g_output.append(groups_names)
    return g_output


def conf_datetime():    # Configuração data e hora fuso horário de São Paulo
    date_time = datetime.datetime.now()
    timedelta = datetime.timedelta(hours=-3)
    timezone = datetime.timezone(timedelta, name='America/Sao_Paulo')
    dt = date_time.astimezone(timezone)
    row_dt = dt.strftime("%Y/%m/%d %H:%M:%S")
    return row_dt

def conf_row(cell, row, items, teams, gol_name):    # Valores das celulas da planilha
    if cell is True:
        row_peso = float(row['Peso (O)']) / 100
        row_atingido = float(row['% Atingido (O)']) / 100
        row_resultado = row_peso * row_atingido
        row_datetime = conf_datetime()
        row_add = [items['name'], teams, gol_name, '{:.2f}'.format(row_peso),
                   '{:.2f}'.format(row_atingido), '{:.2f}'.format(row_resultado), row_datetime]
    else:
        row_corporativo = float(row['% Corporativo']) / 100
        row_pessoal = float(row['% Pessoal']) / 100
        row_datetime = conf_datetime()
        row_add = [items['name'], gol_name, 'NaN', '{:.2f}'.format(row_corporativo),
                   '{:.2f}'.format(row_pessoal), row_datetime]
    return row_add


# Load
def write_spread(up_sheet, sh, name_sheet, values, header):  # Planilha no Google Sheets
    if up_sheet:    # Atualizar planilha (função "up_sheet")
        worksht = sh.worksheet(name_sheet)
        values_list = worksht.col_values(1)
        rworksheet = len(values_list) + 1
        row_increase = rworksheet + len(values_list)
        worksht.update('A{}:J{}'.format(rworksheet, row_increase), values)
        print(f"Planilha {name_sheet} atualizada")

    else:  # Criar planilha (função write_spread)
        worksheet = sh.add_worksheet(title=name_sheet, rows=10000, cols=10)
        try:
            default_worksheet = sh.worksheet("Página1")
            sh.del_worksheet(default_worksheet)
        except:
            try:
                default_worksheet = sh.worksheet("Sheet1")
                sh.del_worksheet(default_worksheet)
            except:
                pass
        worksheet.update('A1:J1', [header])
        worksheet.update('A2:J1000', values)
        print(f"Planilha {name_sheet} criada")


def ETL_OKR_trackage(boards_id, key, target_collum, update=True):   # ETL dos quadros cooporativos
    cell_sheet = True
    header = ['Quadros', 'Times', 'Metas', 'Peso', 'Atingido', 'Resultado', 'Data']
    rows_sheet = []

    # Extract
    for board in boards_id:
        monday_request = get_request(board, key)
        d_r = monday_request.json()  # Visualização da requisição
        if len(d_r['data']['boards']) == 0:
            continue
        items = d_r['data']['boards'][0]
        all_tables = get_table_name(items['groups'])  # Função get_table_name
        sheets = {}

    # Transform
        for target_table in all_tables:
            dic = get_monday(items['items'], target_table, target_collum)  # Função "get_monday"
            sheets[target_table] = dic
        for teams in sheets.keys():  # Acessar o dicionario
            team = sheets[teams]
            for gols in team.keys():    # Atribuindo os valores a planilha do Google sheet
                row = team[gols]
                rows_add = conf_row(cell_sheet, row, items, teams, gols)
                rows_sheet.append(rows_add)

    # Load
    write_spread(update, sheet, 'Quadro - Coorporativo', rows_sheet, header)


def ETL_OKR_pessoais(boards_id, key, target_collum, update=True):   # ETl dos quadros pessoais
    cell_sheet = False
    header = ['Quadro', 'Nome', 'Time', 'Corporativo', 'Pessoal', 'Data']
    rows_sheet = []

    # Extract
    for board in boards_id:
        monday_request = get_request(board, key)
        d_r = monday_request.json()  # Visualização da requisição
        if len(d_r['data']['boards']) == 0:
            continue
        items = d_r['data']['boards'][0]
        all_tables = get_table_name(items['groups'])    # Função get_table_name
        sheets = {}

    # Transform
        for target_table in all_tables:
            dic = get_monday(items['items'], target_table, target_collum)   # Função "get_monday"
            sheets[target_table] = dic
        for teams in sheets.keys():     # Acessar o dicionario
            team = sheets[teams]
            for names in team.keys():    # Atribuindo os valores a planilha do Google sheet
                row = team[names]
                rows_add = conf_row(cell_sheet, row, items, teams, names)
                rows_sheet.append(rows_add)

    # Load
    write_spread(update, sheet, 'Quadro-Pessoal', rows_sheet, header)


# Config variables
if __name__ == '__main__':

    # 'Id' dos quadros trimestrais: Q4 2021, Q2 2022
    trackage_boards_id = ['1754755533', '2526633058']
    team_board_id = ['1965409199']

    # Chave do usuário monday
    apiKey = 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjE1NTk1Nzk2OSwidWlkIjoyNTg2MTk2MiwiaWFkIjoiMjAyMi0wNC0xNFQxOToyNjoyNC4wMDBaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NDA3NDE5NSwicmduIjoidXNlMSJ9.c7Z5sqrJvk_eXdjuOI9cpj0wFPITHxwKmuMgFbm2KZw'

    # Selecionar colunas
    trackage_target_collum = ['Peso (O)', '% Atingido (O)']
    team_target_collum = ['% Corporativo', '% Pessoal']

    # Credenciais google sheets
    creds = "./credentials.json"
    gc = gspread.oauth(credentials_filename=creds)
    update_sheet = False

    try:   # Atualizar planilha no Google sheet
        sheet = gc.open('OKR - Trackage')
        update_sheet = True
        print('Atualizando Documento')
    except:  # Criar planilha no Google sheet
        sheet = gc.create('OKR - Trackage')
        print('Criando Documento')
        
    ETL_OKR_trackage(trackage_boards_id, apiKey, trackage_target_collum, update_sheet)
    ETL_OKR_pessoais(team_board_id, apiKey, team_target_collum, update_sheet)

    
    
